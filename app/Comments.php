<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Str;

class Comments extends Model
{
    public function post()
    {
        return $this->belongsTo('App\Posts');
    }
    protected $fillable = ['content'.'posts_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'text';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(empty($model->
                {
                    $model->getKeyName()
                }))
            {
                $model->{$model->getKeyName()}=Str::uuid();
            }

        });
    }
}
