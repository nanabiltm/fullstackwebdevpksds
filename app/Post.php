<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Str;

class Post extends Model
{
    
    protected $fillable = ['title','description'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(empty($model->
                {
                    $model->getKeyName()
                }))
            {
                $model->{$model->getKeyName()}=Str::uuid();
            }

        });
    }
public function comments()
{
    return $this->hasMany('App\Comment');
}
    
    
}
