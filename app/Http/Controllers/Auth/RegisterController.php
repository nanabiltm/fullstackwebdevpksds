<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, 
        [
            'name' => 'required',
            'email' => 'required|unique:user.email|email',
            'username' => 'required'
        ]);

        if($validator->fails()) 
        {
            return response()->json($validator->errors(),400);
        }
    }
}
